from operator import contains
import png
import csv
import json
import numpy as np
from colour import Color as Colour

def rbga_to_colour(rbga):
    return Colour(rgb=rbga[:3])

tile_width = 8
tile_height = 8

def read_tileset(filename):
    with open(filename, 'rb') as tileset_png:
        tileset_reader = png.Reader(file=tileset_png)
        tileset_data = tileset_reader.asRGBA()
        tileset = np.vstack(map(np.uint16, tileset_data[2])) / 255

    tileset_shape = tileset.shape
    tileset_pixel_width = tileset_shape[1] // 4
    tileset_pixel_height = tileset_shape[0]
    tileset_width = tileset_pixel_width // 8
    tileset_height = tileset_pixel_height // 8
    tileset_count = tileset_width * tileset_height

    tileset = np.reshape(tileset, (tileset_height, tile_height, tileset_width, tile_width, 4))
    tileset = np.stack(tileset, axis=1)
    tileset = np.stack(tileset, axis=2)
    tileset = np.reshape(tileset, (tileset_count, tile_height, tile_width, 4))

    return tileset

def process_tileset(tileset):
    tileset = np.reshape(tileset, (-1, tile_width, 4))

    palette = [Colour('black')] # Insert black as first colour for border
    patterns = bytearray()
    colours = bytearray()
    for row_idx, row in enumerate(tileset):
        row_pattern = []
        row_colours = []
    
        for pixel in row:
            pixel_colour = rbga_to_colour(pixel)
            if pixel_colour not in palette:
               palette.append(pixel_colour)
            palette_index = palette.index(pixel_colour)
            if palette_index not in row_colours:
                row_colours.append(palette_index)
            row_pattern.append(row_colours.index(palette_index))

        while len(row_colours) < 2:
            row_colours.append(0)

        if not all([this_pattern == 0 or this_pattern == 1 for this_pattern in row_pattern]):
            print(f'Too many colours in row {row_idx}')
            assert(0)
        assert(len(row_colours) == 2)

        for row_colour in row_colours:
            if not row_colour < 16:
                print('Too many colours in subtile set')
                assert(0)

        row_bit_pattern = 0
        for bit in row_pattern:
            row_bit_pattern = row_bit_pattern << 1 | bit
        patterns += int(row_bit_pattern).to_bytes(1, 'big')

        row_colours_packed = row_colours[1] << 4 | row_colours[0] << 0
        row_colours_packed = int(row_colours_packed)
        colours += row_colours_packed.to_bytes(1, 'big')

    while len(palette) < 16:
        palette.append(Colour('black'))
    palette_bytes = bytearray()
    for c in palette:
        green = round(c.green * 7)
        red = round(c.red * 7)
        blue = round(c.blue * 7)
        colour_num = green << 8 | red << 4 | blue << 0
        palette_bytes += colour_num.to_bytes(2, 'big')
    palette = palette_bytes

    return patterns, colours, palette

tileset = read_tileset('assets/8x8 Dungeon.png')
patterns, colours, palette = process_tileset(tileset)

with open('assets/out/demo1_patterns.bin', 'wb') as pattern_bin:
    pattern_bin.write(patterns)

with open('assets/out/demo1_colours.bin', 'wb') as colours_bin:
    colours_bin.write(colours)

with open('assets/out/demo1_palette.bin', 'wb') as palette_bin:
    palette_bin.write(palette)

with open('assets/out/1_1.csv', newline='') as map_csv:
    map_reader = csv.reader(map_csv)

    tile_map = []
    for row in map_reader:
        row_map = []
        for cell in row:
            row_map.append(cell)
        tile_map.append(row_map)

    tile_map = np.reshape(tile_map, (24, -1, 32))
    tile_map = np.stack(tile_map, axis=1)

    with open('assets/out/demo1_map_1_1.bin', 'wb') as tile_map_bin:
        for page in tile_map:
            for row in page:
                for cell in row:
                    cell = int(cell)
                    if cell < 0:
                        cell = 0 # TODO: Set sane default
                    tile_map_bin.write(int(cell).to_bytes(1, 'big'))
