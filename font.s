    section .rodata

map_1_1_patterns::
    incbin "assets/out/demo1_patterns.bin"
map_1_1_colours::
    incbin "assets/out/demo1_colours.bin"
map_1_1_layout::
    incbin "assets/out/demo1_map_1_1.bin"
map_1_1_palette::
    incbin "assets/out/demo1_palette.bin"