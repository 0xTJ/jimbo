#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdalign.h>
#include <string.h>
#include <machine.h>
#include "v99x8/v99x8.h"

#define PATTERN_GENERATOR_TABLE         0x00000
#define SPRITE_PATTERN_GENERATOR_TABLE  0x01800
#define SPRITE_ATTRIBUTE_TABLE          0x01E00
#define COLOR_TABLE                     0x02000
#define PATTERN_LAYOUT_TABLE            0x03800

#define DISPLAY_PAGES_TMP 2

const short pix_per_tile = 8;
const short subpix_per_pix = 16;
const short subpix_per_tile = subpix_per_pix * pix_per_tile;
const short sprite_pix = 8;

typedef long time;
typedef int kin_val;

const size_t map_1_1_layout_page_count = DISPLAY_PAGES_TMP;
extern uint8_t map_1_1_layout[DISPLAY_PAGES_TMP][24][32];

extern uint8_t map_1_1_patterns[0x100][8];
extern uint8_t map_1_1_colours[0x100][8];
extern uint16_t map_1_1_palette[16];  // TODO: Ensure endiannes

struct world_layout {
    size_t layout_page_count;
    uint8_t (*layout)[24][32];
};

struct world {
    uint8_t (*patterns)[0x100][8];
    uint8_t (*colours)[0x100][8];
    struct world_layout *world_layout;
    uint16_t (*palette)[16];
};

struct world_layout map_1_1_world_layout = {
    map_1_1_layout_page_count,
    map_1_1_layout
};

struct world map_1_1_world = {
    &map_1_1_patterns,
    &map_1_1_colours,
    &map_1_1_world_layout,
    &map_1_1_palette
};

kin_val gravity = subpix_per_pix * 1 / 4;

void load_patterns(uint8_t patterns[0x100][8], uint8_t colours[0x100][8]);
void load_sprites(uint8_t patterns[0x100][8]);
void load_map(unsigned short page_count, uint8_t map[page_count][24][32]);
void dirty_all_pages(void);
void dirty_page(unsigned short page);
void update_tile(unsigned short page, uint8_t x, uint8_t y, uint8_t tile);
void load_palette(uint16_t palette[16]);
void update_screen(size_t scroll);

struct anim_state {
    uint8_t tile;
    uint8_t frames_to_hold;
    uint8_t next_anim_state;
};

enum object_facing {
    OBJECT_FACING_NONE = 0,
    OBJECT_FACING_RIGHT,
    OBJECT_FACING_LEFT,
};

struct object {
    // Animation
    struct anim_state *states;
    uint8_t current_anim_state;
    uint8_t frame_counter;

    // Behaviour
    void (*do_behaviour)(struct object *this);

    // Movement
    kin_val pos_x;
    kin_val pos_y;
    kin_val vel_x;
    kin_val vel_y;

    // State
    bool is_in_air;
    enum object_facing facing;
};


void update_anim_states(struct object *this, struct anim_state *new_states) ;
void tick_anim_states(struct object *this);

void object_update_motion(struct object *this, time dt, kin_val acc_x, kin_val acc_y);
void object_do_collision(struct object *this);

void object_update_motion(struct object *this, time dt, kin_val acc_x, kin_val acc_y) {
    // Half velocity step
    this->vel_x += acc_x * dt / 2;
    this->vel_y += acc_y * dt / 2;

    // Full position step
    this->pos_x += this->vel_x * dt;
    this->pos_y += this->vel_y * dt;

    // Half velocity step
    this->vel_x += acc_x * dt / 2;
    this->vel_y += acc_y * dt / 2;
}

struct anim_state player_anim_states_standing_right[] = {
    {8, 60, 1},
    {9, 60, 2},
    {10, 60, 3},
    {11, 60, 0},
};

struct anim_state player_anim_states_standing_left[] = {
    {12, 60, 1},
    {13, 60, 2},
    {14, 60, 3},
    {15, 60, 0},
};

struct anim_state player_anim_states_running_right[] = {
    {24, 5, 1},
    {25, 5, 2},
    {26, 5, 3},
    {27, 5, 0},
};

struct anim_state player_anim_states_running_left[] = {
    {28, 5, 1},
    {29, 5, 2},
    {30, 5, 3},
    {31, 5, 0},
};

struct anim_state player_anim_states_jumping_right[] = {
    {40, 255, 0},
};

struct anim_state player_anim_states_jumping_left[] = {
    {44, 255, 0},
};

struct anim_state player_anim_states_jumping_far_right[] = {
    {41, 255, 0},
};

struct anim_state player_anim_states_jumping_far_left[] = {
    {45, 255, 0},
};

struct player_anim_states_facing {
    struct anim_state *right;
    struct anim_state *left;
};

struct player_anim_states {
    struct player_anim_states_facing standing;
    struct player_anim_states_facing running;
    struct player_anim_states_facing jumping;
    struct player_anim_states_facing jumping_far;
};

struct player_anim_states player_anim_states = {
    .standing = { player_anim_states_standing_right, player_anim_states_standing_left },
    .running = { player_anim_states_running_right, player_anim_states_running_left },
    .jumping = { player_anim_states_jumping_right, player_anim_states_jumping_left },
    .jumping_far = { player_anim_states_jumping_far_right, player_anim_states_jumping_far_left },
};

const kin_val player_fast_threshold = 20;

void player_update_tick_anim(struct object *this) {
    kin_val vel_x_abs = this->vel_x < 0 ? -this->vel_x : +this->vel_x;
    bool is_facing_left = this->facing == OBJECT_FACING_LEFT;

    struct player_anim_states_facing *anim_states_facing;
    if (this->is_in_air) {
        if (vel_x_abs < player_fast_threshold) {
            anim_states_facing = &player_anim_states.jumping;
        } else {
            anim_states_facing = &player_anim_states.jumping_far;
        }
    } else {
        if (vel_x_abs == 0) {
            anim_states_facing = &player_anim_states.standing;
        } else {
            anim_states_facing = &player_anim_states.running;
        }
    } 
    
    struct anim_state *player_state = is_facing_left ? anim_states_facing->left : anim_states_facing->right;

    update_anim_states(this, player_state);
    tick_anim_states(this);
}

void player_do_behaviour(struct object *this) {
    if (this->pos_y >= subpix_per_tile * 15) {
        if (this->is_in_air) {
            this->pos_y = subpix_per_tile * 15;
            this->vel_y = 0;
            this->is_in_air = false;
        } else {
            this->vel_y = subpix_per_pix * -4;
            this->is_in_air = true;
        }
    }

    object_update_motion(this, 1, 0, gravity);

    player_update_tick_anim(this);
}

struct object player = {
    // Animation
    .states = NULL,
    .current_anim_state = 0,
    .frame_counter = 0,

    // Behaviour
    .do_behaviour = player_do_behaviour,

    // Movement
    .pos_x = 15 * subpix_per_tile,
    .pos_y = 15 * subpix_per_tile,
    .vel_x = 10,
    .vel_y = 0,
};

bool video_setup(void) {
    unsigned char id = (v99x8_status_register_1_read() & V99X8_STATUS_REGISTER_1_ID_MASK) >> V99X8_STATUS_REGISTER_1_ID_SHIFT;
    if (id != V99X8_ID_V9958) {
        printf("Incorrect video ID: %u\n", id);
        return false;
    }

    // Set mode G3, disable interrupts, and enable output
    v99x8_mode_r0_write(V99X8_MODE_R0_M4);
    v99x8_mode_r1_write(V99X8_MODE_R1_BL);

    // Set PAL mode, enable sprites, and use 64k-long RAM chips
    v99x8_mode_r9_write(V99X8_MODE_R9_NT);
    v99x8_mode_r8_write(V99X8_MODE_R8_VR);

    // Setup tables
    v99x8_pattern_generator_table_write(        (PATTERN_GENERATOR_TABLE        | 0x01FFF) >> 11);
    v99x8_sprite_pattern_generator_table_write( (SPRITE_PATTERN_GENERATOR_TABLE | 0x007FF) >> 11);
    v99x8_sprite_attribute_table_write(         (SPRITE_ATTRIBUTE_TABLE         | 0x003FF) >> 7);
    v99x8_color_table_write(                    (COLOR_TABLE                    | 0x01FFF) >> 6);
    v99x8_pattern_layout_table_write(           (PATTERN_LAYOUT_TABLE | 0x08000 | 0x003FF) >> 10);   // Or with 0x08000 for SP2

    // Set top line displayed to 0
    v99x8_vertical_offset_register_write(0);

    // Set margin to colour 0
    v99x8_text_and_screen_margin_color_write(0x00);

    v99x8_mode_r25_write(V99X8_MODE_R25_CMD | V99X8_MODE_R25_MSK | V99X8_MODE_R25_SP2);

    return true;
}

void load_patterns(uint8_t patterns[0x100][8], uint8_t colours[0x100][8]) {
    for (unsigned char i = 0; i < 3; ++i) {
        v99x8_vram_write(false, PATTERN_GENERATOR_TABLE + i * 0x100 * 8, 0x100 * 8, (void *)patterns);
    }
    for (unsigned char i = 0; i < 3; ++i) {
        v99x8_vram_write(false, COLOR_TABLE + i * 0x100 * 8, 0x100 * 8, (void *)colours);
    }
}

void load_sprites(uint8_t patterns[0x100][8]) {
    v99x8_vram_write(false, SPRITE_PATTERN_GENERATOR_TABLE, 0x100 * 8, (void *)patterns);

    // Reset all sprite colours to a sane default
    uint8_t colours[8];
    for (int i = 0; i < 8; ++i) {
        colours[i] = 0x01;
    }

    for (int i = 0; i < 32; ++i) {
        v99x8_sm2_sprite_attribute_write(SPRITE_ATTRIBUTE_TABLE, i, 211, 0, 0);
    }

    for (int i = 0; i < 16; ++i) {
        v99x8_sm2_sprite_color_write(SPRITE_ATTRIBUTE_TABLE, i, false, colours);
    }
}

void load_map(unsigned short page_count, uint8_t layout[page_count][24][32]) {
    v99x8_vram_write(true, 0, page_count * 32 * 24, (uint8_t *) layout);
}

short prev_even_page = -1;
short prev_odd_page = -1;

void dirty_all_pages(void) {
    prev_even_page = -1;
    prev_odd_page = -1;
}

void dirty_page(unsigned short page) {
    if (page == prev_even_page) {
        prev_even_page = -1;
    }
    if (page == prev_odd_page) {
        prev_odd_page = -1;
    }
}

void update_tile(unsigned short page, uint8_t x, uint8_t y, uint8_t tile) {
    v99x8_vram_write(true, (page * 24 + y) * 32 + x, 1, &tile);
    dirty_page(page);
}

void update_screen(size_t scroll) {
    size_t screen_total_tile_scroll = scroll / pix_per_tile;
    size_t screen_page_scroll = screen_total_tile_scroll / 32;
    size_t screen_tile_scroll = screen_total_tile_scroll % 64;
    size_t screen_bit_scroll = scroll % pix_per_tile;

    v99x8_horizontal_scroll_write(screen_tile_scroll * pix_per_tile + screen_bit_scroll);

    short this_even_page = (screen_page_scroll + 1) / 2 * 2;
    short this_odd_page = (screen_page_scroll + 2) / 2 * 2 - 1;

    if (this_even_page >= DISPLAY_PAGES_TMP) {
        this_even_page %= DISPLAY_PAGES_TMP;
        this_even_page += DISPLAY_PAGES_TMP % 2;
    }
    if (this_odd_page >= DISPLAY_PAGES_TMP) {
        this_odd_page %= DISPLAY_PAGES_TMP;
        this_odd_page += DISPLAY_PAGES_TMP % 2;
    }

    if (this_odd_page != prev_odd_page) {
        uint16_t src = this_odd_page * 24 * 32;         // In ExpRAM
        uint16_t dst = PATTERN_LAYOUT_TABLE | 0x08000;  // In VRAM
        uint16_t cnt = 24 * 32;
        v99x8_lmmm_ext(
            src, dst, cnt,
            V99X8_ARGUMENT_MXD_VRAM | V99X8_ARGUMENT_MXS_EXPRAM | V99X8_ARGUMENT_DIY_DOWN | V99X8_ARGUMENT_DIX_RIGHT,
            V99X8_LOGICAL_OPERATION_IMP
        );
        prev_odd_page = this_odd_page;
    }

    if (this_even_page != prev_even_page) {
        uint16_t src = this_even_page * 24 * 32;        // In ExpRAM
        uint16_t dst = PATTERN_LAYOUT_TABLE | 0x00000;  // In VRAM
        uint16_t cnt = 24 * 32;
        v99x8_lmmm_ext(
            src, dst, cnt,
            V99X8_ARGUMENT_MXD_VRAM | V99X8_ARGUMENT_MXS_EXPRAM | V99X8_ARGUMENT_DIY_DOWN | V99X8_ARGUMENT_DIX_RIGHT,
            V99X8_LOGICAL_OPERATION_IMP
        );
        prev_even_page = this_even_page;
    }

    v99x8_wait_ce();
}

void load_palette(uint16_t palette[16]) {
    for (unsigned i = 0; i < 16; ++i) {
        v99x8_palette_register_write(i, palette[i]);
    }
}

void wait_for_vretrace(void) {
    while (v99x8_status_register_2_read() & V99X8_STATUS_REGISTER_2_VR) {
        // Loop
    }
}

void wait_for_not_vretrace(void) {
    while (!(v99x8_status_register_2_read() & V99X8_STATUS_REGISTER_2_VR)) {
        // Loop
    }
}

void update_anim_states(struct object *this, struct anim_state *new_states) {
    if (this->states != new_states) {
        this->states = new_states;
        this->current_anim_state = 0;
        this->frame_counter = 0;
    }
}

void tick_anim_states(struct object *this) {
    while (this->frame_counter == 0) {
        if (!this->states) {
            break;
        }

        uint8_t next_state = this->states[this->current_anim_state].next_anim_state;
        this->current_anim_state = next_state;
        this->frame_counter = this->states[next_state].frames_to_hold;
    }

    this->frame_counter -= 1;
}

const unsigned screen_center_x = 16 * pix_per_tile;
const unsigned screen_center_y = 15 * pix_per_tile - 1;

bool is_jumping = false;

void kmain() {
    video_setup();

    load_palette(*map_1_1_world.palette);
    load_patterns(*map_1_1_world.patterns, *map_1_1_world.colours);
    load_sprites(*map_1_1_world.patterns);
    load_map(map_1_1_world.world_layout->layout_page_count, map_1_1_world.world_layout->layout);
    dirty_all_pages();

    for (unsigned i = 0;; ++i) {
        wait_for_not_vretrace();
        wait_for_vretrace();

        player.do_behaviour(&player);

        int player_pix_pos_x = player.pos_x / subpix_per_pix;
        int player_pix_pos_y = player.pos_y / subpix_per_pix;

        int scroll_x = player_pix_pos_x > screen_center_x ? player_pix_pos_x - screen_center_x : 0;
        update_screen(scroll_x);

        int screen_player_pos_x = player_pix_pos_x - scroll_x;
        int screen_player_pos_y = player_pix_pos_y;
        if (screen_player_pos_y >= 212 || screen_player_pos_y <= -sprite_pix) {
            screen_player_pos_y = 212;
        }

        uint8_t tile = player.states[player.current_anim_state].tile;
        v99x8_sm2_sprite_attribute_write(SPRITE_ATTRIBUTE_TABLE, 0, screen_player_pos_y, screen_player_pos_x, tile);
    }

    while (1)
        ;
}
